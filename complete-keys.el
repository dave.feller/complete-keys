;;; complete-keys.el --- Completing read for keymaps -*- lexical-binding: t -*-

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;;; Alternative to embark-prefix-help

;;; Code:

(require 'embark)

(defcustom complete-keys-bindings (list "M-<iso-lefttab>")
  "List of keybindings for to try binding `complete-keys' to when
`complete-keys-mode' is enabled. Keys are only bound to `complete-keys'
when they are not already bound."
  :type '(list string)
  :group 'complete-keys)

(defcustom complete-keys-prefix-help-command nil
  "If non-nil use `complete-keys' as `prefix-help-command'."
  :type 'boolean
  :group 'complete-keys)

(defun complete-keys--get-bindings (prefix map)
  (let ((prefix-map (if (= 0 (seq-length prefix))
                        map
                      (keymap-lookup map (key-description prefix))))
        binds)
    (cond
     ((or (null prefix-map) (numberp prefix-map)))
     ((keymapp prefix-map)
      (map-keymap
       (lambda (key def)
         (cond
          ((and (numberp key)
                (= key 27)
                (keymapp def))
           (map-keymap
            (lambda (key2 def2)
              (unless (memq def (list 'undefined 'self-insert-command 'digit-argument
                                      'negative-argument 'embark-keymap-help nil))
                (push (cons (vconcat (vector key key2)) def2) binds)))
            def))
          (t (push (cons (vector key) def) binds))))
       (keymap-canonicalize prefix-map))))
    (nreverse binds)))

;; `embark--formatted-bindings' almost
(defun complete-keys--formatted-bindings (map)
  "Return the formatted keybinding of KEYMAP.
The keybindings are returned in their order of appearance.
If NESTED is non-nil subkeymaps are not flattened."
  (let* ((commands
          (cl-loop for (key . def) in map
                   for name = (embark--command-name def)
                   for cmd = (keymap--menu-item-binding def)
                   unless (memq cmd '(nil embark-keymap-help
                                          negative-argument
                                          digit-argument
                                          self-insert-command
                                          undefined))
                   collect (list name cmd key
                                 (concat
                                  (if (eq (car-safe def) 'menu-item)
                                      "menu-item"
                                    (key-description key))))))
         (width (cl-loop for (_name _cmd _key desc) in commands
                         maximize (length desc)))
         (default)
         (candidates
          (cl-loop for item in commands
                   for (name cmd key desc) = item
                   for desc-rep =
                   (concat
                    (propertize desc 'face 'embark-keybinding)
                    (and (embark--action-repeatable-p cmd)
                         embark-keybinding-repeat))
                   for formatted =
                   (propertize
                    (concat desc-rep
                            (make-string (- width (length desc-rep) -1) ?\s)
                            name)
                    'embark-command cmd)
                   when (equal key [13])
                   do (setq default formatted)
                   collect (cons formatted item))))
    (cons candidates default)))

(defun complete-keys-up ()
  (interactive)
  (throw 'up t))

;;;###autoload
(defun complete-keys (prefix map)
  "Complete key sequence beginning with current prefix keys using `completing-read'."
  (interactive
   (let* ((prefix (seq-subseq (this-command-keys-vector) 0 -1)))
     (list prefix (make-composed-keymap (current-active-maps t)))))
  (let* ((enable-recursive-minibuffers t)
         (cand+def (complete-keys--formatted-bindings
                    (complete-keys--get-bindings prefix map)))
         (cand (car cand+def))
         (def (cdr cand+def))
         (prompt (if (> (length prefix) 0)
                     (concat "Key: " (key-description prefix) "- ")
                   "Key: "))
         (choice (catch 'up
                   (minibuffer-with-setup-hook
                       (lambda ()
                         (let ((map (make-sparse-keymap)))
                           (set-keymap-parent map (current-local-map))
                           (define-key map (kbd "M-DEL") 'complete-keys-up)
                           (use-local-map map)))
                     (completing-read
                      prompt
                      (embark--with-category 'embark-keybinding cand)
                      nil nil)))))
    (if (eq choice t)
        (let ((prefix (seq-subseq prefix 0 -1)))
          (complete-keys (if (and (> (length prefix) 0)
                                  (= 27 (elt prefix (1- (length prefix)))))
                             ;; This was a meta bind so we need to
                             ;; remove the ESC key as well
                             (seq-subseq prefix 0 -1)
                           prefix)
                         map))
      (pcase (assoc choice cand)
        (`(,_formatted ,_name ,cmd ,key ,_desc)
         (if (keymapp cmd)
             (complete-keys (vconcat prefix key) map)
           (call-interactively cmd)))))))

(defun complete-keys--init ()
  (complete-keys-local-mode 1))

(define-minor-mode complete-keys-local-mode
  "Minor mode to enable `complete-keys' bindings in the current buffer's
`global-map'."
  :group 'complete-keys
  (if complete-keys-local-mode
      (dolist (key complete-keys-bindings)
        (unless (keymap-lookup global-map key)
          (keymap-global-set key 'complete-keys)))
    (dolist (key complete-keys-bindings)
      (when (eq 'complete-keys (keymap-global-lookup key))
        (keymap-global-unset key t)))))

;;;###autoload
(define-globalized-minor-mode complete-keys-mode
  complete-keys-local-mode complete-keys--init
  :group 'complete-keys
  (if complete-keys-mode
      (when complete-keys-prefix-help-command
        (setq complete-keys--prefix-cmd-backup prefix-help-command)
        (setq prefix-help-command 'complete-keys))
    (when (eq prefix-help-command 'complete-keys)
      (setq prefix-help-command complete-keys--prefix-cmd-backup))))

(provide 'complete-keys)
